" plugins.vim
"
" Maintained by Claud D. Park <posquit0.bj@gmail.com>
" http://www.posquit0.com/


call plug#begin('~/.vim/plugged')

""" Integration {{{
	"" Plugin: Vim Git {{{
    " For syntax highlighting and other Git niceties
    Plug 'tpope/vim-git'
  "" }}}

	"" Plugin: Vim Fugitive {{{
    " The best Git wrapper
    Plug 'tpope/vim-fugitive'
	"" }}}
""" }}}

""" Interface {{{
  "" Plugin: Airline {{{
    " Use statusline more effective
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    if exists('$DISPLAY')
      " Automatic population of the g:airline_symbols dictionary with
      " powerline symbols
      let g:airline_powerline_fonts=1
    endif
    " Specify theme for airline
    let g:airline_theme='tomorrow'
    let g:airline#extensions#tabline#enabled=1
    let g:airline#extensions#tabline#left_sep=''
    let g:airline#extensions#tabline#left_alt_sep='|'
    " Customize a left side of airline
    " let g:airline_section_b='%{strftime('%H:%M:%S')}'
    " Customize a right side of airline
    let g:airline_section_y='[%{&fileformat}/%{strlen(&fenc)?&fenc:&enc}]'
    " Use airline's showmode
    set noshowmode
  "" }}}

	"" Plugin: Vim Startify {{{
    " A fancy start screen for Vim
    Plug 'mhinz/vim-startify'
    " A list of files to bookmark
    let g:startify_bookmarks=[
    \ '~/.vimrc',
    \ '~/.vim/plugins.vim',
    \]
    " A list of Vim regular expressions that filters recently used files
    let g:startify_skiplist=[
    \ 'COMMIT_EDITMSG',
    \ $VIMRUNTIME .'/doc',
    \ 'plugged/.*/doc',
    \ 'bundle/.*/doc',
    \]
  "" }}}
""" }}}

""" Programming {{{
	"" Plugin: Vim Polyglot {{{
    " A collection of language packs for Vim
    Plug 'sheerun/vim-polyglot'
    " No conceal in JSON
    let g:vim_json_syntax_conceal=0
    " Enable syntax highlighting for JSDocs
    let g:javascript_plugin_jsdoc=1
  "" }}}

	"" Plugin: Vim Better Whitespace {{{
    " All trailing whitespace characters to be highlighted
    Plug 'ntpeters/vim-better-whitespace'
    " Disable this plugin for specific file types
    let g:better_whitespace_filetypes_blacklist=['diff', 'gitcommit', 'unite', 'qf', 'help', 'nerdtree']
    " Strip all trailing whitespace everytime save the file
    autocmd BufWritePre * StripWhitespace
  "" }}}

	"" Plugin: Vim Node {{{
    " Tools and environment to make Vim superb for developing with Node.js
    Plug 'moll/vim-node', { 'for': 'javascript' }
  "" }}}

	" Markdown
  "" Plugin: Vim Instant Markdown {{{
    " Instant markdown Previews from Vim
    Plug 'suan/vim-instant-markdown'
    " Only refresh on specific events
    let g:instant_markdown_slow=1
    " Manually control to launch the preview window
    " Command(:InstantMarkdownPreview)
    let g:instant_markdown_autostart=1
  "" }}}
""" }}}

""" Themes {{{
  "" Theme: Molokai {{{
    Plug 'tomasr/molokai'
    " Match the original monokai background color
    let g:molokai_original=1
    " Bring the 256 color version
    let g:rehash256=1
  "" }}}
  "" Theme: Gruvbox {{{
    Plug 'morhetz/gruvbox'
    " Uses 256 color palette
    " Set option value to 16 to fallback
    let g:gruvbox_termcolors=256
    " Change darkmode contrast. Possible values are `soft`, `medium`, `hard`
    let g:gruvbox_contrast_dark='medium'
    " Change lightmode contrast. Possible values are `soft`, `medium`, `hard`
    let g:gruvbox_contrast_light='hard'
    " Change cursor background
    let g:gruvbox_hls_cursor='green'
    " Inverts indent guides
    let g:gruvbox_invert_indent_guides=0
  "" }}}
  "" Theme: Zenburn {{{
    Plug 'jnurmine/Zenburn'
  "" }}}
  "" Theme: Solarized {{{
    Plug 'altercation/vim-colors-solarized'
  "" }}}
  "" Theme: Seoul256 {{{
    Plug 'junegunn/seoul256.vim'
  "" }}}
  "" Theme: Base16 {{{
    Plug 'chriskempson/base16-vim'
  "" }}}
  "" Theme: All-in-One {{{
    Plug 'flazz/vim-colorschemes'
  "" }}}
""" }}}

call plug#end()
