" .vimrc
"
" Maintained by Brad Frost <bfrost2893@gmail.com>

""" General Configuration (except visual.vim)
if filereadable(expand("\~/.vim/general.vim"))
  source \~/.vim/general.vim
endif
if filereadable(expand("\~/.vim/key-mapping.vim"))
  source \~/.vim/key-mapping.vim
endif

""" Plugin Configuration
" All the vim plugins are managed by 'vim-plug'
" List & configuration of plugins separated to two file 'plugins.vim',
" 'plugins.after.vim'.
" It makes this vimrc could also work out-of-box even if not managed by
" dotfiles.
if filereadable(expand("\~/.vim/plugins.vim"))
  source \~/.vim/plugins.vim
endif

""" General Configuration - Visual
if filereadable(expand("\~/.vim/visual.vim"))
  source \~/.vim/visual.vim
endif

""" Local Specific Configuration
" Use local vimrc if available
if filereadable(expand("\~/.vimrc.local"))
  source \~/.vimrc.local
endif
